import boto3

def get_private_ips(asg_name):
    ec2_client = boto3.client('ec2')
    asg_client = boto3.client('autoscaling')

    response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])
    asg = response['AutoScalingGroups'][0]

    if asg['DesiredCapacity'] > 1:
        instance_ids = [instance['InstanceId'] for instance in asg['Instances']]
        response = ec2_client.describe_instances(InstanceIds=instance_ids)
        instances = [res for res in response['Reservations'] for res in res['Instances']]
        private_ips = [instance['PrivateIpAddress'] for instance in instances]
        return ','.join(private_ips)
    else:
        filters = [{'Name': 'tag:Key1', 'Values': ['Value1']},
                   {'Name': 'tag:Key2', 'Values': ['Value2']},
                   {'Name': 'tag:Key3', 'Values': ['Value3']}]
        response = ec2_client.describe_instances(Filters=filters)
        instance = response['Reservations'][0]['Instances'][0]
        return instance['PrivateIpAddress']

# Replace 'your-asg-name' with the actual name of your Auto Scaling Group
asg_name = 'your-asg-name'
private_ips = get_private_ips(asg_name)
print(private_ips)

