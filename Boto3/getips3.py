import boto3

def get_private_ips_of_instances(asg_tags):
    # Create an EC2 resource client
    ec2_client = boto3.resource('ec2')

    # Get the Auto Scaling Groups matching the provided tags
    asg_client = boto3.client('autoscaling')
    response = asg_client.describe_auto_scaling_groups(Filters=asg_tags)

    # Iterate over each Auto Scaling Group
    for asg in response['AutoScalingGroups']:
        # Get the instances associated with the Auto Scaling Group
        instances = asg['Instances']

        # If the ASG has more than one instance
        if len(instances) > 1:
            private_ips = []
            
            # Iterate over each instance and get its private IP address
            for instance in instances:
                ec2_instance = ec2_client.Instance(instance['InstanceId'])
                private_ips.append(ec2_instance.private_ip_address)
            
            # Print the comma-separated list of private IP addresses
            print(','.join(private_ips))
        else:
            # Print the private IP address of the single instance
            ec2_instance = ec2_client.Instance(instances[0]['InstanceId'])
            print(ec2_instance.private_ip_address)


# Example usage: Provide the ASG tags to filter
asg_tags = [
    {'Name': 'key', 'Values': ['tag1']},
    {'Name': 'value', 'Values': ['value1']}
]

get_private_ips_of_instances(asg_tags)

