import boto3

def get_asg_private_ips(asg_name, tags):
    ec2_client = boto3.client('ec2')
    asg_client = boto3.client('autoscaling')

    response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])
    asg = response['AutoScalingGroups'][0]  # Assuming there is only one ASG with the provided name

    instance_ids = [instance['InstanceId'] for instance in asg['Instances']]
    filters = []

    for tag in tags:
        key, value = tag.split('=')
        filters.append({
            'Name': 'tag:' + key,
            'Values': [value]
        })

    response = ec2_client.describe_instances(InstanceIds=instance_ids, Filters=filters)

    if len(response['Reservations']) == 0:
        print('No instances found with the provided tags.')
    else:
        private_ips = []
        for reservation in response['Reservations']:
            for instance in reservation['Instances']:
                private_ips.append(instance['PrivateIpAddress'])

        if len(private_ips) == 1:
            print('Private IP:', private_ips[0])
        elif len(private_ips) > 1:
            print('Private IPs:', ', '.join(private_ips))

# Example usage
asg_name = 'my-auto-scaling-group'
tags = ['environment=production', 'application=backend']

get_asg_private_ips(asg_name, tags)

