import sys
import boto3

def get_private_ips_of_instances(asg_tags):
    # Create an EC2 resource client
    ec2_client = boto3.resource('ec2')

    # Get the Auto Scaling Groups matching the provided tags
    asg_client = boto3.client('autoscaling')
    response = asg_client.describe_auto_scaling_groups(Filters=asg_tags)

    # Initialize an empty list to store the private IP addresses
    private_ips = []

    # Get the instance IDs from the Auto Scaling Groups
    instance_ids = []
    for asg in response['AutoScalingGroups']:
        for instance in asg['Instances']:
            instance_ids.append(instance['InstanceId'])

    # Filter instances based on the instance state
    instances = ec2_client.instances.filter(InstanceIds=instance_ids, Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

    # Iterate over each instance and get its private IP address
    for instance in instances:
        private_ips.append(instance.private_ip_address)

    # Create a variable InstanceIps containing the private IP addresses
    instance_ips = ','.join(private_ips)

    # Print the InstanceIps variable
    print(f"InstanceIps={instance_ips}")


if __name__ == '__main__':
    # Check if the required number of command-line arguments are provided
    if len(sys.argv) < 3:
        print('Usage: python asg_private_ips.py tag1:value1 tag2:value2 [tag3:value3 ...]')
        sys.exit(1)

    # Parse the command-line arguments and create tag filters
    asg_tags = []
    for arg in sys.argv[1:]:
        tag_key, tag_value = arg.split(':')
        asg_tags.append({'Name': 'tag:' + tag_key, 'Values': [tag_value]})

    # Call the function with the provided tags
    get_private_ips_of_instances(asg_tags)

