import sys
import boto3
from datetime import datetime, timedelta, timezone

def get_private_ips_of_instances(asg_tags):
    # Create an EC2 resource client
    ec2_client = boto3.resource('ec2')

    # Get the Auto Scaling Groups matching the provided tags
    asg_client = boto3.client('autoscaling')
    response = asg_client.describe_auto_scaling_groups(Filters=asg_tags)

    # Initialize an empty list to store the private IP addresses
    private_ips = []

    # Get the instance IDs from the Auto Scaling Groups
    instance_ids = []
    for asg in response['AutoScalingGroups']:
        for instance in asg['Instances']:
            instance_ids.append(instance['InstanceId'])

    # Filter instances based on the instance state and launch time
    instances = ec2_client.instances.filter(InstanceIds=instance_ids, Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

    # Get the current time in UTC
    current_time = datetime.now(timezone.utc)

    # Iterate over each instance and get its private IP address
    for instance in instances:
        # Get the launch time of the instance
        launch_time = instance.launch_time

        # Calculate the time difference between current time and launch time
        time_difference = current_time - launch_time

        # Check if the instance has been running for more than 15 minutes
        if time_difference > timedelta(minutes=15):
            private_ips.append(instance.private_ip_address)

    # Print the private IP addresses
    if len(private_ips) > 1:
        # If there are multiple private IPs, print as comma-separated list
        print(','.join(private_ips))
    elif len(private_ips) == 1:
        # If there's only one private IP, print it directly
        print(private_ips[0])
    else:
        # If no private IPs found, print a message
        print("No instances found with the specified tags and running for more than 15 minutes.")


if __name__ == '__main__':
    # Check if the required number of command-line arguments are provided
    if len(sys.argv) < 3:
        print('Usage: python asg_private_ips.py tag1:value1 tag2:value2 [tag3:value3 ...]')
        sys.exit(1)

    # Parse the command-line arguments and create tag filters
    asg_tags = []
    for arg in sys.argv[1:]:
        tag_key, tag_value = arg.split(':')
        asg_tags.append({'Name': 'tag:' + tag_key, 'Values': [tag_value]})

    # Call the function with the provided tags
    get_private_ips_of_instances(asg_tags)

