pipeline {
    agent any

    environment {
        PRIVATE_IPS = ""
    }

    stages {
        stage('Get Private IPs') {
            steps {
                script {
                    // Execute the Python script and capture the output
                    def scriptOutput = sh(script: 'python asg_private_ips.py tag1:value1 tag2:value2', returnStdout: true).trim()
                    
                    // Assign the output to the environment variable
                    env.PRIVATE_IPS = scriptOutput
                }
            }
        }
        stage('Print Private IPs') {
            steps {
                echo "Private IPs: ${env.PRIVATE_IPS}"
            }
        }
    }
}

